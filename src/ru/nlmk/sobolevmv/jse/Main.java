package ru.nlmk.sobolevmv.jse;

import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String[] args) {

    ListMaker listMaker = new ListMaker();
    List<Object> list = new ArrayList<>();

    list.add(new Person("Alex", "Volkov"));
    list.add(new Person("Ira", "Petrova", "i.petrova@gmail.com"));

    for (List<Description> lst : listMaker.makeList(list)) {
      System.out.println(lst.toString());
    }
  }
}
