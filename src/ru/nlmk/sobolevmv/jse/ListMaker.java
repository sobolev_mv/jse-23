package ru.nlmk.sobolevmv.jse;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListMaker {
  public static final Logger logger = Logger.getLogger(ListMaker.class.getName());

  public List<List<Description>> makeList(List<Object> objects) {
    List<List<Description>> result = new ArrayList<>();
    Class<?> clazz1 = objects.get(0).getClass();

    for (Object object : objects) {
      if (clazz1 != object.getClass()){
        throw new IllegalArgumentException();
      }

      Class<?> clazz = object.getClass();

      do {
        List<Description> subResult = new ArrayList<>();
        parseClass(clazz.getDeclaredFields(), subResult, object);
        if (subResult.size() > 0) result.add(subResult);
        clazz = clazz.getSuperclass();
      } while (clazz.getSuperclass() != null);
    }
    return result;
  }

  private void parseClass(Field[] fields, List<Description> subResult, Object object) {
    for (Field field : fields) {
      boolean hasValue = false;
      try {
        field.setAccessible(true);
        if (field.get(object) != null) {
          hasValue = true;
        }
        subResult.add(new Description(field.getName(), field.getType().toString(), hasValue));
      } catch (IllegalAccessException e) {
        logger.log(Level.ALL,e.getMessage());
      }
    }
  }
}

